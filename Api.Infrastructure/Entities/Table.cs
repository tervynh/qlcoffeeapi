﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Infrastructure.Entities
{
    public class Table :BaseEntity
    {
        public int Id { get; set; }
        public string? TenBan { get; set; }
        [StringLength(10)]
        [DataType("varchar")]
        public string? ViTri { get; set; }
        [StringLength(20)]
        [DataType("varchar")]
        public string? TrangThai { get; set; }
        [StringLength(50)]
        [DataType("varchar")]
        public string? GhiChu { get; set; }

        public Table()
        {

        }

    }
}
