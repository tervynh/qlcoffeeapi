﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Infrastructure.Entities
{
    public class Info :BaseEntity
    {
        public int Id { get; set; }

        [StringLength(50)]
        [DataType("varchar")]
        public string? TenQuan { get; set; }
        [StringLength(50)]
        [DataType("varchar")]
        public string? DiaChi { get; set; }
        [StringLength(15)]
        [DataType("varchar")]
        public string? Sdt { get; set; }
        [StringLength(50)]
        [DataType("varchar")]
        public string? NguoiPhuTrach { get; set; }
        [StringLength(100)]
        [DataType("varchar")]
        public string? GhiChu { get; set; }

        public Info()
        {

        }

    }
}
