﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Infrastructure.Entities
{
    public class Account : BaseEntity
    {
        public int Id { get; set; }
        [StringLength(20)]
        public string? TenNguoiDung { get; set; }
        [StringLength(50)]
        [DataType("varchar")]
        public string? TenDangNhap { get; set; }
        [StringLength(50)]
        [DataType("varchar")]
        public string? Password { get; set; }

        public Account()
        {

        }

    }
}
