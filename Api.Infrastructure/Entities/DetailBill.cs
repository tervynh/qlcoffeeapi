﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Infrastructure.Entities
{
    public class DetailBill : BaseEntity
    {
        public int Id { get; set; }
        public int TableId { get; set; }
        [ForeignKey(nameof(TableId))]
        public Table? Table { get; set; }
        public int BillId { get; set; }
        [ForeignKey(nameof(BillId))]
        public virtual Bill? Bill { get; set;}
        public int DetailFood { get; set; }
        public int SoLuong { get; set; }
        public int SoTienNhan { get; set; }
        public int SoTienThua { get; set; }
        public int TongTien { get; set; }

        public DetailBill()
        {

        }
    }
}
