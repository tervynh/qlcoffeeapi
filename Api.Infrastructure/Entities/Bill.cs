﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Infrastructure.Entities
{
    public class Bill : BaseEntity
    {
        public int Id { get; set; }
        public DateTime GioVao { get; set; }
        public DateTime GioRa { get; set; }
        public string? TrangThai { get; set; }
        public int TableId { get; set; }
        [ForeignKey(nameof(TableId))]
        public Table? Table { get; set; }

        public Bill()
        {

        }

    }
}
