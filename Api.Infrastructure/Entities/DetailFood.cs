﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Infrastructure.Entities
{
    public class DetailFood : BaseEntity
    {
        public int Id { get; set; }
        public string? TenDoUong { get; set; }
        public int Gia { get; set; }
        public string? MoTa { get; set; }
        public int FoodId { get; set; }
        [ForeignKey(nameof(FoodId))]
        public Food? Food { get; set; }

        public DetailFood()
        {

        }
    }


}
