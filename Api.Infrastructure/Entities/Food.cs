﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Infrastructure.Entities
{
    public class Food :BaseEntity 
    {
        public int Id { get; set; }
        [StringLength(50)]
        [DataType("varchar")]
        public string? LoaiDoUong { get; set; }

        public Food()
        {

        }
    }
}
