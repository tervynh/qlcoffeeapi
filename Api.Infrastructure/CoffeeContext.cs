﻿using Api.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Infrastructure
{
    public class CoffeeContext : DbContext
    {
        private string DEFAULT_SCHEMA = "dbo";
        public DbSet<Account>? Accounts { get; set; }
        public DbSet<Bill>? Bills { get; set; }
        public DbSet<DetailBill>? DetailBills { get; set; }
        public DbSet<DetailFood>? DetailFoods { get; set; }
        public DbSet<Food>? Foods { get; set; }
        public DbSet<Info>? Infos { get; set; }
        public DbSet<Table>? Tables { get; set; }

        //  Ham Dung lop context
        public CoffeeContext(DbContextOptions<CoffeeContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema(DEFAULT_SCHEMA);

            base.OnModelCreating(builder);
        }
    }
}
